const mongoose = require('mongoose')

const connectDB = async () => {
    try {
        const conn = await mongoose.connect('mongodb+srv://dmitras:01234567@cluster0.ejqk4q4.mongodb.net/db1?retryWrites=true&w=majority')

        console.log(`MongoDB connected with ${conn.connection.host}`)
    } catch (error) {
        console.log(error)
        process.exit(1)
    }
};

module.exports = connectDB