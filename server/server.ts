import http from 'http';
import express, { Application } from 'express';
import WebScoket from 'ws';
const dotenv = require('dotenv').config();
import { errorHandler } from './middleware/errorMiddleware';
const connectDB = require('./config/db')
const port = process.env.PORT || 5000

connectDB()

const app: Application = express()
// const server = http.createServer(app)
// const wss = new WebScoket.Server({ server })

// wss.on('connection', socket => {
//     console.log('New client connected!')

//     socket.on('message', message => {
//         wss.emit('message', `${socket.id.substr(0, 2)} said: ${message}`);

//         wss.clients.forEach(client => client.send(message));
//     });

//     socket.on('error', err => socket.send(err));

//     socket.send('This is test message of the WebScoket server');
// });

app.use(express.json())
app.use(express.urlencoded({ extended: false }))

app.use('/api/task', require('./routes/taskRoutes'))

app.use(errorHandler)

app.listen(port, () => console.log(`Server listening on port ${port}!`));
// server.listen(port, () => console.log(`Server is started on port ${port}!`))