import express from 'express';
import { getTasks, setTask, updateTask, deleteTask } from '../controllers/taskController';

const router = express.Router()

router.route('/').get(getTasks).post(setTask);
router.route('/:id').put(updateTask).delete(deleteTask);

module.exports = router