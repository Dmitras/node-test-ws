
import mongoose from 'mongoose'

const taskSchema = new mongoose.Schema ({
    text: {
        type: String,
        required: [true, 'Add text']
    }
}, {
    timestamps: true
})

module.exports = mongoose.model('Task', taskSchema)