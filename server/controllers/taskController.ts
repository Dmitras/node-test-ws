
import expressAsyncHandler from "express-async-handler";
import { Request, Response } from "express";

const asyncHandler = expressAsyncHandler;

const Task = require('../models/taskModel')

// @desc Get tasks
// @route GET /api/task
// access Private
export const getTasks = asyncHandler(async (req: Request, res: Response) => {
    const tasks = await Task.find()
    res.status(200).json(tasks);
})

// @desc Set tasks
// @route POST /api/task/:id
// access Private
export const setTask = asyncHandler(async (req: Request, res: Response) => {
    if (!req.body.text) {
        res.status(400)
        throw new Error('Add a textfield')
    }

    const task = await Task.create({
        text: req.body.text,
    })

    res.status(200).json(task)
});

// @desc Update tasks
// @route PUT /api/task/:id
// access Private
export const updateTask = asyncHandler(async (req: Request, res: Response) => {
    const task = await Task.findById(req.params.id)

    if (!task) {
        res.status(400)

        throw new Error('Task not found')
    }

    const updatedTask = await Task.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
    })
    
    res.status(200).json(updatedTask);
})

// @desc Delete tasks
// @route DELETE /api/task/:id
// access Private
export const deleteTask = asyncHandler(async (req: Request, res: Response) => {
    res.status(200).json({ message: `Delete task ${req.params.id}` });
})